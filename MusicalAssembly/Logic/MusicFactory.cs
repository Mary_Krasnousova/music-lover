﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace Logic
{
    public static class MusicFactory
    {
        public static Assembly CreateMusic()
        {
            Assembly assembly = new Assembly();
            Rock firstrRockComposition = new Rock("Альтернативный рок", "Point of No Return", 3.39);
            assembly.AddMusic(firstrRockComposition);
            Rock secondrRockComposition = new Rock("Христианский рок", "Not Gonna Die", 3.45);
            assembly.AddMusic(secondrRockComposition);
            Classical classicalComposition = new Classical("Ludovico Einaudi", "Walk", 3.27);
            assembly.AddMusic(classicalComposition);
            Symphony symphonyComposition = new Symphony(4, "Людвиг Ван Бетховен", "Симфония №5", 13.09);
            assembly.AddMusic(symphonyComposition);
            Opera operaComposition = new Opera("Cпектакль в четырех действиях с прологом. Основа оперы - известное произведение Гете «Доктор Фауст». Великий ученый и доктор Фауст связался с Мефистофелем. Он предлагает Фаусту продать свою душу дьяволу взамен на беспечную, полную новых знаний, жизнь, которую простые люди не в силах постичь и понять.", "Шарль Франсуа Гуно", "Опера Фауст", 170.0);
            assembly.AddMusic(operaComposition);
            return assembly;
        }
    }
}

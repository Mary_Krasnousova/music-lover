﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;

namespace Logic
{
    public static class MusicCalculator
    {
        public static double GetTotalDuration(Assembly assembly)
        {
            double totalDuration = 0;
            List<AbstractMusic> music = assembly.Music;
            foreach (AbstractMusic musicComposition in music)
            {
                totalDuration += musicComposition.Duration;
            }
            return totalDuration;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Logic;

namespace MusicianUI
{
    public partial class MainForm : Form
    {
        Assembly bestComposition = Data.Assembly;

        public MainForm()
        {
            InitializeComponent();
        }

        public class Data
        {
            static Assembly _music = MusicFactory.CreateMusic();

            public static Assembly Assembly
            {
                get { return _music; }
                set { _music = value; }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddEditForm addEditForm = new AddEditForm();
            addEditForm.ShowDialog();
            bestComposition.AddMusic(addEditForm.Music);
            musicList.DataSource = null;
            musicList.DataSource = bestComposition.Music;
            musicList.DisplayMember = "Name";
            this.Refresh();
            UpdateLabels();            
        }

        public void UpdateLabels()
        {
            durationLabel.Text = String.Format("Duration = {0}", MusicCalculator.GetTotalDuration(bestComposition).ToString());
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            musicList.DataSource = bestComposition.Music;
            musicList.DisplayMember = "Name";
            UpdateLabels();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            AddEditForm edit = new AddEditForm((AbstractMusic)musicList.SelectedItem);
            edit.ShowDialog();
            musicList.DataSource = null;
            musicList.DataSource = bestComposition.Music;
            musicList.DisplayMember = "Name";
            this.Refresh();
            UpdateLabels();
        }

        
    }
}

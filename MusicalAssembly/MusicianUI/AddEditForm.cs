﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Logic;

namespace MusicianUI
{
    public partial class AddEditForm : Form
    {

        string _type = null;
        public AbstractMusic Music { get; set; }

        public AddEditForm()
        {
            InitializeComponent();  
        }

        public AddEditForm(AbstractMusic music)
        {
            InitializeComponent();
            Music = music;
            string type;
            type = Convert.ToString(music.GetType());
            string[] split = type.Split('.');
            type = split[1];
            _type = type;
            Fillfields(type);
        }     

        public void Fillfields(string type)
        {
            DisableFields(type);
            typeComboBox.Enabled = false;
            switch (type)
            {
                case "Classical":
                    {
                        Classical classical = (Classical)Music;
                        nameTextBox.Text = classical.Name;
                        durationTextBox.Text = classical.Duration.ToString();
                        nameOfComposerTextBox.Text = classical.NameOfComposer;
                    } break;
                case "Rock":
                    {
                        Rock rock = (Rock)Music;
                        nameTextBox.Text = rock.Name;
                        durationTextBox.Text = rock.Duration.ToString();
                        rockGenreTextBox.Text = rock.RockGenre;
                    } break;
                case "Opera":
                    {
                        Opera opera = (Opera)Music;
                        nameTextBox.Text = opera.Name;
                        durationTextBox.Text = opera.Duration.ToString();
                        nameOfComposerTextBox.Text = opera.NameOfComposer;
                        shortDescriptionTextBox.Text = opera.ShortDescription;
                    } break;
                case "Symphony":
                    {
                        Symphony symphony = (Symphony)Music;
                        nameTextBox.Text = symphony.Name;
                        durationTextBox.Text = symphony.Duration.ToString();
                        nameOfComposerTextBox.Text = symphony.NameOfComposer;
                        numberOfPartsTextBox.Text = symphony.NumberOfParts.ToString();
                    } break;
            }
        }

        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string type = typeComboBox.SelectedItem.ToString();
            DisableFields(type);
        }

        void EnabledFields()
        {
            nameTextBox.Enabled = true;
            durationTextBox.Enabled = true;
            rockGenreTextBox.Enabled = true;
            nameOfComposerTextBox.Enabled = true;
            shortDescriptionTextBox.Enabled = true;
            numberOfPartsTextBox.Enabled = true;
        }

        void DisableFields(string type)
        {
            EnabledFields();
            switch (type)
            {   

                case "Rock":
                    {
                        nameOfComposerTextBox.Enabled = false;
                        shortDescriptionTextBox.Enabled = false;
                        numberOfPartsTextBox.Enabled = false;
                    } break;
                case "Classical":
                    {
                        shortDescriptionTextBox.Enabled = false;
                        rockGenreTextBox.Enabled = false;
                        numberOfPartsTextBox.Enabled = false;
                    } break;
                case "Opera":
                    {
                        rockGenreTextBox.Enabled = false;
                        numberOfPartsTextBox.Enabled = false;
                    } break;
                case "Symphony":
                    {
                        shortDescriptionTextBox.Enabled = false;
                        rockGenreTextBox.Enabled = false;
                    } break;
            }

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string type = typeComboBox.SelectedItem.ToString();
            switch (type)
            {
                case "Rock":
                    {
                        string name = nameTextBox.Text;
                        double duration = Convert.ToDouble(durationTextBox.Text);
                        string rockGenre = rockGenreTextBox.Text;
                        Music = new Rock(rockGenre, name, duration);
                    } break;
                case "Classical":
                    {
                        string name = nameTextBox.Text;
                        string nameOfComposer = nameOfComposerTextBox.Text;
                        double duration = Convert.ToDouble(durationTextBox.Text);
                        Music = new Classical(nameOfComposer, name, duration);
                    } break;
                case "Opera":
                    {
                        string name = nameTextBox.Text;
                        string nameOfComposer = nameOfComposerTextBox.Text;
                        string shortDescription = shortDescriptionTextBox.Text;
                        double duration = Convert.ToDouble(durationTextBox.Text);
                        Music = new Opera(shortDescription, nameOfComposer, name, duration);
                    } break;
                case "Symphony":
                    {
                        string name = nameTextBox.Text;
                        double duration = Convert.ToDouble(durationTextBox.Text);
                        string nameOfComposer = nameOfComposerTextBox.Text;
                        int numberOfParts = Convert.ToInt32(numberOfPartsTextBox.Text);
                        Music = new Symphony(numberOfParts, nameOfComposer, name, duration);
                    } break;   
            }
            this.Close();

        }

        private void editButton_Click(object sender, EventArgs e)
        {
            switch (_type)
            {
                case "Rock":
                    {
                        Rock rock = (Rock)Music;
                        rock.Name = nameTextBox.Text;
                        rock.Duration = Convert.ToDouble(durationTextBox.Text);
                        rock.RockGenre = rockGenreTextBox.Text;
                    } break;
                case "Classical":
                    {
                        Classical classical = (Classical)Music;
                        classical.Name = nameTextBox.Text;
                        classical.Duration = Convert.ToDouble(durationTextBox.Text);
                        classical.NameOfComposer = nameOfComposerTextBox.Text;
                    } break;
                case "Opera":
                    {
                        Opera opera = (Opera)Music;
                        opera.Name = nameTextBox.Text;
                        opera.Duration = Convert.ToDouble(durationTextBox.Text);
                        opera.NameOfComposer = nameOfComposerTextBox.Text;
                        opera.ShortDescription = shortDescriptionTextBox.Text;
                    } break;
                case "Symphony":
                    {
                        Symphony symphony = (Symphony)Music;
                        symphony.Name = nameTextBox.Text;
                        symphony.Duration = Convert.ToDouble(durationTextBox.Text);
                        symphony.NameOfComposer = nameOfComposerTextBox.Text;
                        symphony.NumberOfParts = Convert.ToInt32(numberOfPartsTextBox.Text);
                    } break;
            }
            this.Close();
        }

        
    }


}

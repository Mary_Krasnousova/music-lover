﻿namespace MusicianUI
{
    partial class AddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.typeLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.musicDurationLabel = new System.Windows.Forms.Label();
            this.nameOfComposerLabel = new System.Windows.Forms.Label();
            this.rockGenreLabel = new System.Windows.Forms.Label();
            this.shortDescriptionLabel = new System.Windows.Forms.Label();
            this.numberOfPartsLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.durationTextBox = new System.Windows.Forms.TextBox();
            this.rockGenreTextBox = new System.Windows.Forms.TextBox();
            this.nameOfComposerTextBox = new System.Windows.Forms.TextBox();
            this.shortDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.numberOfPartsTextBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // typeComboBox
            // 
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Items.AddRange(new object[] {
            "Rock",
            "Classical",
            "Opera",
            "Symphony"});
            this.typeComboBox.Location = new System.Drawing.Point(190, 12);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(121, 21);
            this.typeComboBox.TabIndex = 0;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Location = new System.Drawing.Point(53, 20);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(29, 13);
            this.typeLabel.TabIndex = 1;
            this.typeLabel.Text = "Тип:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(52, 61);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(32, 13);
            this.nameLabel.TabIndex = 2;
            this.nameLabel.Text = "Имя:";
            // 
            // musicDurationLabel
            // 
            this.musicDurationLabel.AutoSize = true;
            this.musicDurationLabel.Location = new System.Drawing.Point(52, 95);
            this.musicDurationLabel.Name = "musicDurationLabel";
            this.musicDurationLabel.Size = new System.Drawing.Size(114, 13);
            this.musicDurationLabel.TabIndex = 3;
            this.musicDurationLabel.Text = "Продолжительность:";
            // 
            // nameOfComposerLabel
            // 
            this.nameOfComposerLabel.AutoSize = true;
            this.nameOfComposerLabel.Location = new System.Drawing.Point(53, 168);
            this.nameOfComposerLabel.Name = "nameOfComposerLabel";
            this.nameOfComposerLabel.Size = new System.Drawing.Size(102, 13);
            this.nameOfComposerLabel.TabIndex = 4;
            this.nameOfComposerLabel.Text = "Имя композитора:";
            // 
            // rockGenreLabel
            // 
            this.rockGenreLabel.AutoSize = true;
            this.rockGenreLabel.Location = new System.Drawing.Point(53, 131);
            this.rockGenreLabel.Name = "rockGenreLabel";
            this.rockGenreLabel.Size = new System.Drawing.Size(58, 13);
            this.rockGenreLabel.TabIndex = 5;
            this.rockGenreLabel.Text = "Рок-жанр:";
            // 
            // shortDescriptionLabel
            // 
            this.shortDescriptionLabel.AutoSize = true;
            this.shortDescriptionLabel.Location = new System.Drawing.Point(53, 205);
            this.shortDescriptionLabel.Name = "shortDescriptionLabel";
            this.shortDescriptionLabel.Size = new System.Drawing.Size(103, 13);
            this.shortDescriptionLabel.TabIndex = 6;
            this.shortDescriptionLabel.Text = "Краткое описание:";
            // 
            // numberOfPartsLabel
            // 
            this.numberOfPartsLabel.AutoSize = true;
            this.numberOfPartsLabel.Location = new System.Drawing.Point(54, 242);
            this.numberOfPartsLabel.Name = "numberOfPartsLabel";
            this.numberOfPartsLabel.Size = new System.Drawing.Size(106, 13);
            this.numberOfPartsLabel.TabIndex = 7;
            this.numberOfPartsLabel.Text = "Количество частей:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(190, 54);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(121, 20);
            this.nameTextBox.TabIndex = 8;
            // 
            // durationTextBox
            // 
            this.durationTextBox.Location = new System.Drawing.Point(190, 92);
            this.durationTextBox.Name = "durationTextBox";
            this.durationTextBox.Size = new System.Drawing.Size(121, 20);
            this.durationTextBox.TabIndex = 9;
            // 
            // rockGenreTextBox
            // 
            this.rockGenreTextBox.Location = new System.Drawing.Point(190, 128);
            this.rockGenreTextBox.Name = "rockGenreTextBox";
            this.rockGenreTextBox.Size = new System.Drawing.Size(121, 20);
            this.rockGenreTextBox.TabIndex = 10;
            // 
            // nameOfComposerTextBox
            // 
            this.nameOfComposerTextBox.Location = new System.Drawing.Point(190, 165);
            this.nameOfComposerTextBox.Name = "nameOfComposerTextBox";
            this.nameOfComposerTextBox.Size = new System.Drawing.Size(121, 20);
            this.nameOfComposerTextBox.TabIndex = 11;
            // 
            // shortDescriptionTextBox
            // 
            this.shortDescriptionTextBox.Location = new System.Drawing.Point(190, 202);
            this.shortDescriptionTextBox.Name = "shortDescriptionTextBox";
            this.shortDescriptionTextBox.Size = new System.Drawing.Size(121, 20);
            this.shortDescriptionTextBox.TabIndex = 12;
            // 
            // numberOfPartsTextBox
            // 
            this.numberOfPartsTextBox.Location = new System.Drawing.Point(190, 239);
            this.numberOfPartsTextBox.Name = "numberOfPartsTextBox";
            this.numberOfPartsTextBox.Size = new System.Drawing.Size(121, 20);
            this.numberOfPartsTextBox.TabIndex = 13;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(25, 295);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(86, 37);
            this.okButton.TabIndex = 14;
            this.okButton.Text = "ОК";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(220, 295);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(91, 37);
            this.closeButton.TabIndex = 15;
            this.closeButton.Text = "Закрыть";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(117, 295);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(97, 37);
            this.editButton.TabIndex = 16;
            this.editButton.Text = "Редактировать";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // AddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 353);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.numberOfPartsTextBox);
            this.Controls.Add(this.shortDescriptionTextBox);
            this.Controls.Add(this.nameOfComposerTextBox);
            this.Controls.Add(this.rockGenreTextBox);
            this.Controls.Add(this.durationTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.numberOfPartsLabel);
            this.Controls.Add(this.shortDescriptionLabel);
            this.Controls.Add(this.rockGenreLabel);
            this.Controls.Add(this.nameOfComposerLabel);
            this.Controls.Add(this.musicDurationLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.typeLabel);
            this.Controls.Add(this.typeComboBox);
            this.Name = "AddEditForm";
            this.Text = "AddEditForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label musicDurationLabel;
        private System.Windows.Forms.Label nameOfComposerLabel;
        private System.Windows.Forms.Label rockGenreLabel;
        private System.Windows.Forms.Label shortDescriptionLabel;
        private System.Windows.Forms.Label numberOfPartsLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox durationTextBox;
        private System.Windows.Forms.TextBox rockGenreTextBox;
        private System.Windows.Forms.TextBox nameOfComposerTextBox;
        private System.Windows.Forms.TextBox shortDescriptionTextBox;
        private System.Windows.Forms.TextBox numberOfPartsTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button editButton;
    }
}
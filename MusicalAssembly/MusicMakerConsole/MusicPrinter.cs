﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Logic;

namespace MusicMakerConsole
{
    class MusicPrinter
    {
        public static void Print(Assembly diskAssembly)
        {
            double totalDuration = MusicCalculator.GetTotalDuration(diskAssembly);
            Console.WriteLine("Продолжительность = {0}", totalDuration);
        }
    }
}

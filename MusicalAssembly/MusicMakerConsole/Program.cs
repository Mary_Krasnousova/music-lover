﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using Logic;

namespace MusicMakerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly bestComposition = MusicFactory.CreateMusic();
            MusicPrinter.Print(bestComposition);
            Console.ReadKey();
        }
    }
}

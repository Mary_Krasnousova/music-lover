﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Assembly
    {
        private List<AbstractMusic> _music = new List<AbstractMusic>();

        public void AddMusic(AbstractMusic composition)
        {
            _music.Add(composition);
        }

        public void DeleteMusic(AbstractMusic composition)
        {
            _music.Remove(composition);
        }

        public List<AbstractMusic> Music
        {
            get { return _music; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Classical : AbstractMusic
    {
        private string _nameOfComposer;

        public string NameOfComposer
        {
            get { return _nameOfComposer; }
            set { _nameOfComposer = value; }
        }

        public Classical(string nameOfComposers, string name, double duration)
            : base(name, duration)
        {
            this._nameOfComposer = nameOfComposers;
        }
    }
}

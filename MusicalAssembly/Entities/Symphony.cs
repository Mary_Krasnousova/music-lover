﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Symphony : Classical
    {
        private int _numberOfParts;

        public int NumberOfParts
        {
            get { return _numberOfParts; }
            set { _numberOfParts = value; }
        }

        public Symphony(int numberOfParts, string nameOfComposers, string name, double duration)
            : base(nameOfComposers, name, duration)
        {
            this._numberOfParts = numberOfParts;
        }
    }
}

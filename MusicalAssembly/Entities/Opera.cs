﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Opera : Classical
    {
        private string _shortDescription;

        public string ShortDescription
        {
            get { return _shortDescription; }
            set { _shortDescription = value; }
        }

        public Opera(string shortDescription, string nameOfComposers, string name, double duration) : base(nameOfComposers, name, duration)
        {
            this._shortDescription = shortDescription;
        }
    }
}

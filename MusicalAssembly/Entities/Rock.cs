﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Rock : AbstractMusic
    {
        private string _rockGenre;

        public string RockGenre
        {
            get { return _rockGenre; }
            set { _rockGenre = value; }
        }

        public Rock(string rockGenre, string name, double duration)
            : base(name, duration)
        {
            this._rockGenre = rockGenre;
        }
    }
}

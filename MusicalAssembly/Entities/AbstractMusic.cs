﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public abstract class AbstractMusic
    {
        private string _name;
        private double _duration;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public AbstractMusic(string name, double duration)
        {
            this._name = name;
            this._duration = duration;
        }
    }
}
